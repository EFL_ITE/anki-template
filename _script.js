function playAudioUK() {
  var audio = document.getElementById("uk-audio");
  if (audio) {
    audio.volume = 0.5;
    audio.play();
    return true;
  }
  return false;
}
function playAudioUS() {
  var audio = document.getElementById("us-audio");
  if (audio) {
    audio.removeEventListener('ended', playAudioUK);
    audio.volume = 0.5;
    audio.play();
    return true;
  }
  return false;
}
function playAudioSequence() {
  var audio = document.getElementById("us-audio");
  if (audio) {
    audio.addEventListener('ended', playAudioUK);
    audio.volume = 0.5;
    audio.play();
  }
}
function toggle_jpn(display = 'none') {
  var eng = document.getElementsByClassName("cht_exp");
  for (i = 0; i < eng.length; i++) {
    eng[i].style.display = display;
    // console.log(eng[i].innerHTML)
  }
  var eng = document.getElementsByClassName("japanese");
  for (i = 0; i < eng.length; i++) {
    eng[i].style.display = display;
    // console.log(eng[i].innerHTML)
  }
}

function open_tab(event, name) {
  var i, tab_content, tab_links;
  tab_content = document.getElementsByClassName("tab_content");
  for (i = 0; i < tab_content.length; i++) {
    tab_content[i].style.display = "none";
  }
  tab_links = document.getElementsByClassName("tab_links");
  for (i = 0; i < tab_links.length; i++) {
    tab_links[i].className = tab_links[i].className.replace(" active", "");
  }
  document.getElementById(name).style.display = "block";
  event.currentTarget.className += " active";
  if (event.currentTarget.id == 'tab_eng') {
    toggle_jpn('none');
  } else {
    toggle_jpn('initial');
  }
}
function cleaner() {
  if (document.querySelector('.pos.dsense_pos')) {
    //document.getElementsByClassName('posgram dpos-g hdib lmr-5')[0].style.display = 'none';
    var pos = document.getElementsByClassName('posgram dpos-g hdib lmr-5');
    if (pos.length) {
      pos[0].style.display = 'none';
    }
  }
  var pos = document.querySelectorAll('h3 .dgram');
  const regex = /\[|\]/gi;
  for (let i = 0; i < pos.length; i++) {
    // console.log(pos[i].innerHTML);
    var res = pos[i].innerHTML.replace(regex, "");
    pos[i].innerHTML = res;
    // console.log(res);
  }
}

function highlighter(text, query) {
  if (!text) return text;
  let suffix = 3;

  if (query.endsWith("y")) {
    query = query.slice(0, -1);
  }
  else if (query.endsWith("e")) {
    query = query.slice(0, -1);
  }
  const regex = new RegExp(`\\b${query}[a-z]{0,3}\\b`, 'gi');
  text = text.replace(regex, `<span class='highlight'>$&</span>`);
  return text;
}

function search() {
  const query = document.getElementById('expression').innerHTML.trim();
  var examp = document.getElementsByClassName('eg');
  for (let i = 0; i < examp.length; i++) {
    if (examp[i].getElementsByClassName('highlight').length) break
    var text = examp[i].textContent;
    examp[i].innerHTML = highlighter(text, query);
  }
}